from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
        url(r'test$','auditory_model_neuron.views.filter_model'),
        url(r'test_T$','auditory_model_neuron.views.filter_model',{'upload_type':'default_T'}),
        url(r'test_A$','auditory_model_neuron.views.filter_model',{'upload_type':'default_A'}),
    # Examples:
    # url(r'^$', 'rpi_server.views.home', name='home'),
    # url(r'^rpi_server/', include('rpi_server.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
