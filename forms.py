from django import forms

class rf_and_wav_form(forms.Form):
    json_rf = forms.CharField()
    wav_file = forms.FileField(required=False)
