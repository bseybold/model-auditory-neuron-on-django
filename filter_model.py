'''
.. module:: model_neuron
  :platform: all
  :synopsis: given a receptive field and stimulus, model a neural response
.. moduleauthor:: Bryan Seybold

model_neuron.py
filter_model.py (aka)

A collection of functions useful for simulating a neuron with multiple receptive
fields. Tools include functions to convert sound stimuli to a compatible
representation and functions to model a neurons response.
'''
from pylab import specgram
import numpy
from scipy.ndimage.filters import gaussian_filter
from scipy import interpolate
from brian import *
from brian.hears import *

def selective_spectrogram(input,fs,frequencies):
    '''Generate a spectrogram at particular frequencies

    Computes the spectrogram, then selects the listed frequencies from it

    Args:
        input (numpy.ndarray): the waveform of the stimulus
        fs (number): the sampling rate of the waveform
        frequencies (iterable numbers): a list of the frequencies to collect the
            spectrogram at.

    Returns:
        a three tuple of numpy arrays: (spectrogram, frequency_indicies, time_axis)
            spectrogram: the spectrogram at each frequency
            frequency_indicies: the index closes to the requested frequencies
            time_axis: the time axis for the spectrogram
    '''
    Pxx, freqs, t, handle = specgram(input.reshape(len(input)),NFFT =
            512,Fs=fs,sides='onesided',noverlap=128)
    freq_inds = copy(frequencies)
    for i,f in enumerate(frequencies):
        f_ind = find(min(abs(freqs-f)) == abs(freqs-f))
        freq_inds[i] = f_ind
    return Pxx[numpy.int16(freq_inds),:],freq_inds,t

def convert_wav(input,cfmin=4000*Hz,cfmax=32000*Hz,cfn=25,method=None):
    '''convert a .wav file to a spectrogram with log spacing

    Computes frequencies of interest using :func:`brian.hears.erbspace()`

    Args:
        input (anything input :class:`brian.hears.Sound`): the .wav file
        cfmin (number * :class:`brian.Hz`): the bottom frequency for the specgram
            defualt: 4000*Hz
        cfmax (number * :class:`brian.Hz`): the top frequency for the spectrogram
            defualt: 32000*Hz
        cfm (integer): the number of frequencies to produce for the spectrogram
            default: 25
        method (**None**,*'gammatone'*): how to compute the spectrogram
            values: **None**, *default*, compute by picking single frequencies
                from the linear spectrogram
                    *'gammatone'*, compute with a gammatone filter bank
    Returns:
        output from :func:`selective_spectrogram`
                 or :func:`brian.hears.gammatone.process()`
    '''
    sound = Sound(input)
    cf = erbspace(cfmin,cfmax,cfn)
    #cf = arange(cfmin,cfmax,(cfmax-cfmin)/float(cfn))
    if method is 'gammatone':
        gfb = gammatone(sound, cf)
        rval = (gfb.process(),gfb.cf,sound.times)
        return  rval
    return selective_spectrogram(sound,sound.samplerate,cf)

def convert_matrix_to_frames(matrix,columns_to_append):
    '''convert a matrix to a frames to prepare for convolution

    duplicates a shifted version of the matrix and appends along another dimension.
    If different dimensions represent the course of time, this corresponds to
    taking a time window, converting it to a vector, and doing so for each time
    window in the stimulus. If the original matrix is N x M, and
    columns_to_append is d, the output is N*d x M-d.

    Args:
        matrix (numpy.ndarray): the 2d matrix to convert
        columns_to_append (integer): number of time steps to include to make wider
    Returns:
        matrix (numpy.ndarray): the converted matrix

    >>> mat = convert_matrix_to_frames(numpy.array([[1,0],[0,1],[0,0]]))
    >>> numpy.all(rf == numpy.array([[1,0,0,1],[0,1,0,0]]))
    True
    '''
    if len(matrix.shape) != 2:
        raise ValueError('matrix must be a 2d matrix')
    stim_frames = empty([matrix.shape[1]*columns_to_append,
                         matrix.shape[0]-(columns_to_append-1)])
    for i in xrange(int(columns_to_append)):
        d1_start = i*matrix.shape[1]
        d1_end   = (i+1)*matrix.shape[1]
        stim_frames[d1_start:d1_end,:] = (
                matrix[i:matrix.shape[0]-(columns_to_append-1-i),:].T)
    return stim_frames

def convert_square_rf_to_vector(rf_matrix):
    '''convert an rf_matrix to an rf_vector for convolution

    calls :func:`convert_matrix_to_frames` on the input and converts to a vector

    Args:
        rf_matrix(numpy.ndarray): the 2d matrix to convert
    Returns:
        vector (numpy.ndarray): the 1d receptive field equivalent

    >>> rf = convert_square_rf_to_vector(numpy.array([[1,0][0,1]]))
    >>> numpy.all(rf == numpy.array([1,0,0,1]))
    True
    '''
    if len(rf_matrix.shape) != 2:
        raise ValueError('rf_matrix must be a 2d matrix')
    rf_vec = convert_matrix_to_frames(rf_matrix,rf_matrix.shape[1])
    rf_vec.shape = (numpy.max(rf_vec.shape),)
    return rf_vec


def convolve_rf_and_stimulus(rf_vec,stim_matrix):
    '''convert a rf and stimulus to the same form and convolve them

    If rf_vec is 2d, convert to a 1d receptive field vector
    The length of the 1d receptive field vector must be an even multiple
    of the number of rows in the stimulus matrix

    Args:
        rf_vec(numpy.ndarray): 1d (Nd) or 2d (Nxd) receptive field
        stim_matrix(numpy.ndarray): 2d stimulus matrix (NxM)
    Returns:
        projection(numpy.ndarray): the dot product of the two inputs
    '''
    if (len(rf_vec.shape) > 2 or len(stim_matrix.shape) != 2):
        raise ValueError(
            'rf_vec must be an N vector and stim_matrix must be cNxM')
    if len(rf_vec.shape) == 2:
        rf_vec = convert_square_rf_to_vector(rf_vec)
    #convert stimulus matrix to frames, does nothing if same length
    if rf_vec.shape[0]%stim_matrix.shape[1] != 0:
        raise ValueError(
            'The length of the rf_vec must be an even multiple of '
            'the stimulus_matrix second dimension size')
    stim_frames = convert_matrix_to_frames(stim_matrix,
                                        rf_vec.shape[0]/stim_matrix.shape[1])
    #if stim_matrix is set up in frames appropriately, it's this simple
    return rf_vec.dot(stim_frames)

def pass_through_nonlinearity(filter_output,nl_x,nl_y):
    '''determines a nonlinear response

    makes use of :func:`numpy.interpolate.interp1d`

    Args:
        filter_output (numpy.ndarray): 1d vector
        nl_x (numpy.ndarray): input coordinates to build non-linearity from
        nl_y (numpy.ndarray): output coordinates to build non-linearity from
    Returns:
        output (numpy.ndarray): response to the input

    >>>in = numpy.array([0,0,0.5,1])
    >>>out = pass_through_nonlinearity(in,[0,0.5,1],[-1,0,2])
    >>>numpy.all(out == [-1,-1,0,2])
    True
    '''
    #the output of convolve rf and stimulus
    f = interpolate.interp1d(nl_x,nl_y)
    return f(filter_output)

def rates_to_poisson(rates,sampling_rate):
    '''converts poisson rates to random events

    Args:
        rates (numpy.ndarray): 1d vector of poisson rates (in Hz)
        sampling_rate (numpy.ndarray): to compute bin sizes in second
    Returns:
        events (numpy.ndarray): a boolean array where True indicates an event
            and False indicates no event for each bin in rates.
    '''
    probabilities = rates/float(sampling_rate)
    cutoffs = numpy.random.random_sample(rates.shape)
    return (probabilities > cutoffs)

##if __name__ == '__main__':
##import CRM_reader
##w = CRM_reader.get_waveform('Talker 0','Ringo','Blue','Five')
##o,f,t = convert_wav(w,20*Hz,10*kHz,2000)
#import scipy.io.wavfile
#import pylab
#pylab.figure()
#(fs,w) = scipy.io.wavfile.read('audio_test_T.wav')
#o,f,t = convert_wav(w[:,0],50*Hz,8000*Hz,20)

#pylab.figure()
#pylab.cla()
#pylab.imshow(log10(o),aspect='auto')
##pylab.imshow(o.T)
#pylab.gca().invert_yaxis()
##pylab.gca().axis('auto')
#pylab.show()

#rf_matrix = numpy.zeros((20,20))
#rf_matrix[1:4,:] = 1
#rf_matrix[4:7,:] = -1
##rf_matrix = rf_matrix.T
#stim_matrix = log10(o)-numpy.median(log10(o))

#conv = convolve_rf_and_stimulus(rf_matrix,stim_matrix.T)

#rf_nl_x = [conv.min(),0,conv.max()]
#rf_nl_y = [0,0,conv.max()]
#nlo = pass_through_nonlinearity(conv,rf_nl_x,rf_nl_y)
#sampling_rate = 1/0.5*nlo.mean()
#spikes = rates_to_poisson(nlo,sampling_rate)

#pylab.figure()
#raster = spikes.reshape((1,spikes.shape[0]))
#pylab.imshow(raster,cmap=pylab.cm.binary,origin='lower',extent=[0,spikes.shape[0],-1,0],interpolation='none',aspect='auto')
#pylab.plot(nlo,color=(0.3,1,0.3),linewidth=2)
#pylab.show()

###test rf bits
##rf_matrix = numpy.array([[1,-1],[0,0]])
##stim_matrix = numpy.array([[1,-1],[0,0],[1,0],[1,0]])
##rf_nl_x = [0,2]
##rf_nl_y = [0,1]
##sampling_rate = 1
##conv = convolve_rf_and_stimulus(rf_matrix,stim_matrix)
##nlo  = pass_through_nonlinearity(conv,rf_nl_x,rf_nl_y)
##trials = numpy.zeros((100,3))
##for x in xrange(trials.shape[0]):
##    spikes = rates_to_poisson(nlo,sampling_rate)
##    trials[x,:] = spikes
##print trials.mean()

###test longer rf bits
##rf_matrix = numpy.random.random_sample((20,20))
##stim_matrix = numpy.random.random_sample((20,2000))
##conv = convolve_rf_and_stimulus(rf_matrix,stim_matrix.T)
##rf_nl_x = [conv.min(),conv.max()]
##rf_nl_y = [0,1]
##nlo = pass_through_nonlinearity(conv,rf_nl_x,rf_nl_y)
##sampling_rate = 1/0.1*nlo.mean()
##trials = numpy.zeros((100,nlo.shape[0]))
##for x in xrange(trials.shape[0]):
##    spikes = rates_to_poisson(nlo,sampling_rate)
##    trials[x,:] = spikes
##print trials.mean()
