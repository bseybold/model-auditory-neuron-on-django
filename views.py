# Create your views here.

from django.http import HttpResponse
from django.template import Context, loader, RequestContext
from django.shortcuts import render_to_response
from logging import getLogger
import json
import tempfile


logger = getLogger('rpi_server')
sound_dir = '/home/bryan/python_dev/django/rpi_server/auditory_model_neuron/static/'

from forms import rf_and_wav_form
from adapter import rf_adapter

def filter_model(request,upload_type='file'):
    if request.method == "POST":
        logger.info('new post request')
        logger.info(str(request.POST))
        logger.info(str(request.FILES))
        form = rf_and_wav_form(request.POST, request.FILES)
        if form.is_valid():
            logger.info(str('valid form'))
            json_array = form.cleaned_data['json_rf']
            rf_array = json.loads(json_array)
            #logger.info(rf_array
            #for chunk in request.FILES['wav_file'].chunks():
            #    logger.info(chunk)
            #with open('/home/bryan/python_dev/django/run/last_upload.txt','wb') as destination:
            import os
            logger.info(str(os.getcwd()))
            if upload_type is 'file':
                destination = tempfile.TemporaryFile()
                for chunk in request.FILES['wav_file'].chunks():
                    destination.write(chunk)
                destination.seek(0)
            elif upload_type is 'default_T':
                destination = open(sound_dir+'audio_test_T.wav','r')
            elif upload_type is 'default_A':
                destination = open(sound_dir+'audio_test_A.wav','r')
            try:
                r_val = rf_adapter(rf_array,destination)
                #logger.info(r_val)
                r_val = json.dumps(r_val)
            except Exception as e:
                logger.info(e)
            destination.close()
            return HttpResponse(r_val)
        else:
            logger.info(str('invalid form'))
            return HttpResponse('FAIL')
    else:
        form = rf_and_wav_form()
        data = {'form':form}
        return render_to_response('auditory_rf.html',data,
                context_instance=RequestContext(request))
