from filter_model import *
import scipy.io.wavfile
import numpy
import logging

logger = logging.getLogger('rpi_server')

def rf_adapter(rf_array,wav_file_handle):
    #logger.info('test_part')
    #logger.info(str(wav_file_handle))
    #logger.info(1)
    #logger.info(wav_file_handle)
    #logger.info(2)
    #wav_file_handle.seek(0)
    #logger.info(3)
    #return wav_file_handle.read()
    
    #return ' '.join((str(rf_array),wav_file_handle.read()))

    #rf_matrix = numpy.zeros((10,10))
    #rf_matrix[1:3,:] = 1
    #rf_matrix[3:5,:] = -1
    #rf_matrix = rf_matrix.T
    rf_matrix = numpy.array(rf_array)
    rf_matrix = rf_matrix[:,::-1]

    (fs, w) = scipy.io.wavfile.read(wav_file_handle)
    o,f,t = convert_wav(w[:,0],50*Hz,16000*Hz,10)
    stim_matrix = log10(o)-numpy.median(log10(o))
    conv = convolve_rf_and_stimulus(rf_matrix, stim_matrix.T)

    rf_nl_x = [conv.min(),0,conv.max()]
    rf_nl_y = [0,0,conv.max()]
    nlo = pass_through_nonlinearity(conv,rf_nl_x,rf_nl_y)
    sampling_rate = 1/0.5*nlo.mean()
    spikes = rates_to_poisson(nlo,sampling_rate)
    
    short_stim_matrix = stim_matrix[:,stim_matrix.shape[1]-nlo.size:]
    times = numpy.arange(0,short_stim_matrix.shape[1]*512/float(fs),512.0/float(fs))
    return {'stim_matrix':short_stim_matrix.tolist(),
            'non_linearity_output':nlo.tolist(),
            'spike_output':spikes.tolist(),
            'time_list':times.tolist(),
            }
